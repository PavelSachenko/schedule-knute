<?php
require_once 'vendor/autoload.php';
use app\schedule\Schedule;
use bot;
//work
$option = settingURI();
if($option == 'app'){
    $api = new Schedule();
    echo $api->run();
}elseif ($option == 'bot'){
    $bot = new Bot();
    $bot->run();
}else{
    echo 'Page not found 404';
}

function settingURI(){
    $option =  $_SERVER['REQUEST_URI'];
    $option = substr($option, 1, strlen($option));
    $option = substr($option, 0, strpos($option, '/'));
    $_SERVER['REQUEST_URI'] = substr($_SERVER['REQUEST_URI'], strlen($option)+1, strlen($_SERVER['REQUEST_URI']));
    return $option;
}