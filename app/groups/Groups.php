<?php


namespace app\groups;


use app\API;
use app\lib\DataBase;

class Groups extends API
{

    protected function indexAction()
    {

        // TODO: Implement indexAction() method.
    }

    protected function viewAction($parameters)
    {
        DataBase::getPDO();
        //SELECT groups.title FROM `groups` INNER JOIN faculties ON faculty_id = faculties.id WHERE faculties.title = 'fit' AND form_education = 'bakalavr' AND course = 3
        $data['groups'] = DataBase::requestDB("SELECT groups.title FROM groups
                        INNER JOIN faculties ON faculty_id = faculties.id 
                        WHERE faculties.title = :faculty 
                        AND form_education = :form
                        AND course = :course", ['faculty' => $parameters[0], 'course' => $parameters[1], 'form' => $parameters[2]]);
        return $this->response($data, 200);

    }

    protected function createAction()
    {
        // TODO: Implement createAction() method.
    }

    protected function updateAction()
    {
        // TODO: Implement updateAction() method.
    }

    protected function deleteAction()
    {
        // TODO: Implement deleteAction() method.
    }
}