<?php

namespace app\lib;

use PDO;
use FFI\Exception;

abstract class DataBase
{
    private static $pdo = null;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public static function getPDO()
    {

        if (null === self::$pdo) {
            $config_db = require 'app/config/config_db.php';
            $dsn = $config_db['driver'] . ':host=' . $config_db['host'] . ';dbname=' . $config_db['db_name'] . ';charset=' . $config_db['charset'];
            self::$pdo = new PDO($dsn, $config_db['db_user'], $config_db['db_password']);

        }
        return self::$pdo;
    }

    public static function requestDB($sql = '', $param = [], $modification = 'all')
    {

        try {
            $statement = self::$pdo->prepare($sql);
            $statement->execute($param);
            if ($modification == 'all') {
                return $statement->fetchAll(PDO::FETCH_ASSOC);
            } else {
                if ($modification == 'one') {
                    return $statement->fetchColumn();
                }
            }
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

}