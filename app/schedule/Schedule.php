<?php


namespace app\schedule;


use app\API;
use app\lib\DataBase;

class Schedule extends API
{

    private function checkWeek(){
        $day = date('l');
        $week = strtotime(date("m.d.y"));
        $week =  date("W", $week);
        ($week%2 == 0) ? $week = 2 : $week = 1;
        if($day == 'Saturday' || $day == 'Sunday')
            ($week == 1) ? $week = 2 : $week = 1;
        return $week;
    }

    private function createDay($week){
        $day = date('l') . " " .$week;
        if($day == 'Sunday '. $week || $day == 'Saturday '.$week){
            $day = 'Monday '. $week;
        }
        return $day;
    }
    public function indexAction()
    {

        return $this->response(['data' => 'empty data'], 404);
    }

    protected function viewAction($parameters)
    {
        $data['week'] = $this->checkWeek();
        if (isset($parameters[4])){
            if(strtolower($parameters[4]) == 'tomorrow')
                $data['day'] = date("l", strtotime("+1 day")) . " " . $data['week']; // на завтра
            else
                $data['day'] = $parameters[4] . " " . $data['week'];
        }else{
            $data['day'] = $this->createDay($data['week']);
        }
        DataBase::getPDO();
        $data['schedule'] = DataBase::requestDB("SELECT subjects.title, subjects.number_lesson FROM faculties
             INNER JOIN groups ON groups.faculty_id = faculties.id INNER JOIN schedules ON schedules.group_id = groups.id
            INNER JOIN subjects ON subjects.schedule_id = schedules.id WHERE faculties.title = :faculty
            AND groups.course = :course
            AND groups.title = :group
            AND schedules.day = :day
            AND groups.form_education = :form", ['faculty' => $parameters[0],
            'course' => $parameters[1],
            'group' => $parameters[2],
            'form' => $parameters[3],
            'day' => $data['day']]);


        return $this->response($data, 200);
    }

    protected function createAction()
    {
        return $this->response(['success' => 'data was added'], 200);
        // TODO: Implement createAction() method.
    }

    protected function updateAction()
    {
        return $this->response(['success' => 'data was updated'], 200);
        // TODO: Implement updateAction() method.
    }

    protected function deleteAction()
    {
        // TODO: Implement deleteAction() method.
    }
}