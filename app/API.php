<?php


namespace app;




abstract class API
{
    protected $apiName = ''; //users
    protected $method = ''; //GET|POST|PUT|DELETE
    protected $requestUri = [];
    private $action = ''; //Название метод для выполнения
    protected $error = [];

    public function __construct() {
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        header("Content-Type: application/json");

        //Массив GET параметров разделенных слешем
        $this->apiName = strtolower( substr($_SERVER['REQUEST_URI'], 1, strpos($_SERVER['REQUEST_URI'],'/', 1)-1));
        $_SERVER['REQUEST_URI'] = substr($_SERVER['REQUEST_URI'], strlen($this->apiName)+1);
        $this->requestUri= explode('/', trim($_SERVER['REQUEST_URI'],'/'));
        $this->requestUri[2] = urldecode($this->requestUri[2]);

        $this->requestParams = $_REQUEST;
        //Определение метода запроса
        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception("Unexpected Header");
            }
        }
    }

    public function run() {

        $path =  $this->apiName . '/' . ucfirst($this->apiName) ;
        if(file_exists(__DIR__ . '/' . $path. '.php')){
            $this->action = $this->getAction();
            $path = str_replace('/', '\\', $path);
            $class = '\\app\\'. $path;
            $class = new $class();
            if (method_exists($class, $this->action)) {
               return $class->{$this->action}($this->requestUri);
            } else {
                throw new RuntimeException('Invalid Method', 405);
            }
        }else{
            array_push($this->error, 'API not found 404');
            //$this->response(null, 404);
            echo 'page not found ff';
        }

    }

    protected function response($data, $status = 500) {
        header("HTTP/1.1 " . $status . " " . $this->requestStatus($status));
        if($this->error)
            return json_encode($this->error);
        return json_encode($data);
    }

    private function requestStatus($code) {
        $status = array(
            200 => 'OK',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        );
        return ($status[$code])?$status[$code]:$status[500];
    }

    protected function getAction()
    {
        $method = $this->method;
        switch ($method) {
            case 'GET':
                if($this->requestUri){
                    return 'viewAction';
                } else {
                    return 'indexAction';
                }
                break;
            case 'POST':
                return 'createAction';
                break;
            case 'PUT':
                return 'updateAction';
                break;
            case 'DELETE':
                return 'deleteAction';
                break;
            default:
                return null;
        }
    }

    abstract protected function indexAction();
    abstract protected function viewAction($parameters);
    abstract protected function createAction();
    abstract protected function updateAction();
    abstract protected function deleteAction();
}