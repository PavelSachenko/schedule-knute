<?php
require_once 'vendor/autoload.php';

use app\lib\DataBase;
use Telegram\Bot\Api;

$telegram = new Api('1204828712:AAEwXWuuwBLaSimKjFpB-RhIBNzYhuDT2XE');
DataBase::getPDO();
$result = $telegram->getWebhookUpdate();
(!empty($result['message']['text'])) ? $text = $result['message']['text'] : $text = '';
$chatId = $result['message']['chat']['id'];
(!empty($result['message']['from']['username'])) ? $name = $result['message']['from']['username'] : $name = 'null';
(!empty($result['message']['from']['first_name'])) ? $firstName = $result['message']['from']['first_name'] : $firstName = 'null';
(!empty($result['message']['from']['last_name'])) ? $lastName = $result['message']['from']['last_name'] : $lastName = 'null';
$command = null;
$query = $result['callback_query']['data'];

$callback_query = $result['callback_query'];
$data = $callback_query['data'];
$message = ['callback_query']['message'];
$message_id = ['callback_query']['message']['message_id'];
$chat_id_in = $callback_query['message']['chat']['id'];

$chat_data = require 'chat_data.php';
$text = substr($text, 0, (strripos($text, '@') != false) ? strripos($text, '@') : strlen($text));

switch ($text){
    case '/start':
        if( DataBase::requestDB("SELECT chat_id FROM _chats WHERE chat_id = '$chatId'") == null)
            DataBase::requestDB("INSERT INTO _chats (chat_id, first_name, last_name, username) VALUES ($chatId, '$firstName', '$lastName', '$name')");
        $textResponse = "Бот создан для удобного просмотра расписания\n\nДля пользователей: \n" .
            "1. Первым делом нужно зарегестрировать бота для этого пропишите /settings\n" .
            "2. После прохождения регистрации - вы можете воспользоваться навигацией с помощью команды /commands\n" .
            "3. Если возникли ошибки или вопросы напишите мне: @PashaSachenko\n\n" ;

        $telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => $textResponse,
        ]);
        break;
    case '/settings':
        if( DataBase::requestDB("SELECT chat_id FROM _chats WHERE chat_id = '$chatId'") == null)
            DataBase::requestDB("INSERT INTO _chats (chat_id, first_name, last_name, username) VALUES ($chatId, '$firstName', '$lastName', '$name')");
        DataBase::requestDB("UPDATE _chats SET faculty = null, course = null, form_education = null, title_group = null WHERE chat_id = $chatId");
        $arr = getFacultyButtons();
        $keyboard = [
            'inline_keyboard' =>
                [
                    array_slice($arr, 0, 3),
                    array_slice($arr, 3),
            ]
        ];
        $encodedKeyboard = json_encode($keyboard);
        $parameters = [
            'chat_id' => $chatId,
            'text' => 'Нужно выбрать факультет:' . $data . " : " . $chat_data['faculty'],
            'reply_markup' => $encodedKeyboard,
        ];
        $telegram->sendMessage($parameters);
        break;
    case '/today':
        $lessons = lessonAdd();
        $parameters = [
            'chat_id' => $chatId,
            'text' => $lessons,
        ];
        $telegram->sendMessage($parameters);
        break;
    case '/tomorrow':
        $lessons = lessonAdd('tomorrow');
        $parameters = [
            'chat_id' => $chatId,
            'text' => $lessons,
        ];
        $telegram->sendMessage($parameters);
        break;
    case '/monday':
        $lessons = lessonAdd('Monday');
        $parameters = [
            'chat_id' => $chatId,
            'text' => $lessons,
        ];
        $telegram->sendMessage($parameters);
        break;
    case '/tuesday':
        $lessons = lessonAdd('Tuesday');
        $parameters = [
            'chat_id' => $chatId,
            'text' => $lessons,
        ];
        $telegram->sendMessage($parameters);
        break;
    case '/wednesday':
        $lessons = lessonAdd('Wednesday');
        $parameters = [
            'chat_id' => $chatId,
            'text' => $lessons,
        ];
        $telegram->sendMessage($parameters);
        break;
    case '/thursday':
        $lessons = lessonAdd('Thursday');
        $parameters = [
            'chat_id' => $chatId,
            'text' => $lessons,
        ];
        $telegram->sendMessage($parameters);
        break;
    case '/friday':
        $lessons = lessonAdd('Friday');
        $parameters = [
            'chat_id' => $chatId,
            'text' => $lessons,
        ];
        $telegram->sendMessage($parameters);
        break;
    case '/lesson_call':
        $textResponse = "1я пара: 8:20 - 9:40 \n2я пара: 10:05 - 11:25 \n3я пара: 12:05 - 13:25 \n4я пара: 13:50 - 15:10";
        $textResponse .= "\n5я пара: 15:25 - 16:45 \n6я пара: 17:00 - 18:20 \n7я пара: 18:30 - 19:50";
        $telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => $textResponse,
        ]);
        break;
    case '/commands':
        $textResponse = "/today - расписание на сегодня \n/tomorrow - расписание на завтра \n/monday - расписание на понедельник";
        $textResponse .= "\n/tuesday - расписание на вторник \n/wednesday - расписание на среду \n/thursday - расписание на четверг";
        $textResponse .= "\n/friday - расписание на пятницу\n/lesson_call - расписание звонков";
        $telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => $textResponse,
        ]);
        break;
    case '/help':
        $textResponse = "Бот создан для удобного просмотра расписания\n\nДля пользователей: \n" .
                        "1. Первым делом нужно зарегестрировать бота для этого пропишите /settings\n" .
                        "2. После прохождения регистрации - вы можете воспользоваться навигацией с помощью команды /commands\n" .
                        "3. Если возникли ошибки напишите мне: @PashaSachenko\n\n" .
                        "Для программистов, которые, возможно, хотят использовать API:\n" .
                        "Как взять список групп: \n".
                        "Список групп вернется при запросе GET на этот сайт:\n".
                        "https://pavel-test-knute.website/app/groups/ФАКУЛЬТЕТ/КУРС/СТЕПЕНЬ/\n" .
                        "Для примера: \n" .
                        "https://pavel-test-knute.website/app/groups/fit/3/bakalavr/ - вернет все название групп \n" .
                        "Факультеты: fit, femp, ftm, frgtb, ffo, fmtp\n" .
                        "Курсы: 1, 2, 3, 4 - для степени 'bakalavr', 1 - для степени 'magistr'\n" .
                        "Степени: bakalavr, magistr\n\n" .
                        "Как взять расписание:\n" .
                        "Список расписания вернется при запросе GET на этот сайт:\n" .
                        "https://pavel-test-knute.website/app/schedule/ФАКУЛЬТЕТ/КУРС/ГУРППА/СТЕПЕНЬ/*НЕОБЕЗАТЕЛЬНЫЙ_ПАРАМЕТР*\n" .
                        "Для примера: https://pavel-test-knute.website/app/schedule/fit/3/8/bakalavr/ \n" .
                        "Вернет расписание на текущую неделю, номер недели, день \n" .
                        "Какие есть необезательные параметры: \n" .
                        "today - расписание на сегодня\ntomorrow - распиание на завтра\nmonday - расписание на понедельник\n" .
                        "tuesday - распиание на вторник\nwednesday - расписание на среду\nthursday - расписание на четверг\n" .
                        "friday - расписание на пятницу\n\nГлавное помнить что, расписание идет на текущую неделю, то есть, " .
                        "если сейчас вторая неделя и сегодня пятница, выполнив запрос на понедельник мы получим расписание второй недели на понедельник, " .
                        "а не первой. Возможно, я переделаю, ну комон, мне никто не платит :)\n\n" .
                        "Если возникли вопросы пишите мне: @PashaSachenko, кстате ищу человека, который умеет во frontend - " .
                        "что бы пилить совместные проекты, я на backend";
        $telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => $textResponse,
        ]);
        break;

}

if($chat_data['faculty'] == null){

    DataBase::requestDB("UPDATE _chats SET faculty = '$data' WHERE chat_id = '$chat_id_in'");

    $keyboard = [
        'inline_keyboard' => [
            [
                ['text' => 'bakalavr', 'callback_data' => 'bakalavr'],
                ['text' => 'magistr', 'callback_data' => 'magistr']
            ]
        ]
    ];
    $encodedKeyboard = json_encode($keyboard);
    $telegram->sendMessage([
        'chat_id' => $chat_id_in,
        'text' => 'Выберите форму обучения:',
        'reply_markup' => $encodedKeyboard,

    ]);
}elseif ($chat_data['form_education'] == null){
    DataBase::requestDB("UPDATE _chats SET form_education = '$data' WHERE chat_id = '$chat_id_in'");
    if ($data == 'magistr'){
        $buttons = [
            ['text' => '1', 'callback_data' => '1'],
        ];

    }else{
        $buttons = [
            ['text' => '1', 'callback_data' => '1'],
            ['text' => '2', 'callback_data' => '2'],
            ['text' => '3', 'callback_data' => '3'],
            ['text' => '4', 'callback_data' => '4'],
        ];
    }

    $keyboard = [
        'inline_keyboard' => [
            $buttons
        ]
    ];
    $encodedKeyboard = json_encode($keyboard);
    $telegram->sendMessage([
        'chat_id' => $chat_id_in,
        'text' => 'Выберите курс: ',
        'reply_markup' => $encodedKeyboard,

    ]);
}elseif ($chat_data['course'] == null){
    DataBase::requestDB("UPDATE _chats SET course = $data WHERE chat_id = '$chat_id_in'");
    $arr = addKeys($chat_id_in);
    $keyboard = [
        'inline_keyboard' => $arr
    ];
    $encodedKeyboard = json_encode($keyboard);
    $telegram->sendMessage([
        'chat_id' => $chat_id_in,
        'text' => 'Выберите группу: ',
        'reply_markup' => $encodedKeyboard,

    ]);
}elseif ($chat_data['title_group'] == null){
    DataBase::requestDB("UPDATE _chats SET title_group = '$data' WHERE chat_id = '$chat_id_in'");
    $telegram->sendMessage([
        'chat_id' => $chat_id_in,
        'text' => 'Настройка завершена, список команд, которые помогут в навигации расписания: /commands',
    ]);
}



function getFacultyButtons(){
    $arr = [];
    $data = DataBase::requestDB("SELECT title FROM faculties limit 6");
    foreach ($data as $item) {
        array_push($arr, ['text' => $item['title'], 'callback_data' => $item['title']]);
    }
    return $arr;
}


function lessonAdd($day = ''){
    $lessons = '';
    global $chatId;
    DataBase::getPDO();
    $faculty = DataBase::requestDB("SELECT faculty FROM _chats WHERE chat_id = $chatId")[0]['faculty'];
    $course = DataBase::requestDB("SELECT course FROM _chats WHERE chat_id = $chatId")[0]['course'];
    $group = DataBase::requestDB("SELECT title_group FROM _chats WHERE chat_id = $chatId")[0]['title_group'];

    $form = DataBase::requestDB("SELECT form_education FROM _chats WHERE chat_id = $chatId")[0]['form_education'];


    $request = file_get_contents("https://pavel-test-knute.website/app/schedule/$faculty/$course/$group/$form/". $day);
    $request = json_decode($request, true);
    $lessons .= "*****************\n";
    foreach ($request['schedule'] as $item){
        $lessons .= $item['number_lesson']. "я - пара \n";
        if(strpos($item['title'], '|') != null){
            $lesson1 = substr($item['title'], 0, strpos($item['title'], '|'));
            $lesson2 = substr($item['title'], strpos($item['title'], '|')+1, strlen($item['title']));
            textAdd($lessons, $lesson1);
            $lessons .= "-----------------------\n";

            textAdd($lessons, $lesson2);

        }else{
            textAdd($lessons, $item['title']);
        }
        $lessons .= "*****************\n";

    }
    if($lessons == '')
        $lessons = "******* Возможно пары нема -_(0-0)_- *\n";

    return $lessons;
}

function textAdd(&$text, $lesson){
    $lesson1 = trim(substr($lesson, 0,strpos($lesson, '#')));
    $cut = strpos($lesson, '#')+1;
    if($cut == 1)
        $cut = 0;
    $lessonTeacher1 = trim(substr($lesson, $cut, strlen($lesson)));
    $text .= $lessonTeacher1. "\n";
    $text .= $lesson1. "\n";
}

function addKeys($chatId,$arr = [], $offset = 0){
    DataBase::getPDO();
    $bufferArr = [];
    $faculty = DataBase::requestDB("SELECT faculty FROM _chats WHERE chat_id = $chatId")[0]['faculty'];
    $formEducation = DataBase::requestDB("SELECT form_education FROM _chats WHERE chat_id = $chatId")[0]['form_education'];
    $cource = DataBase::requestDB("SELECT course FROM _chats WHERE chat_id = $chatId")[0]['course'];

    $keys =  DataBase::requestDB("SELECT groups.title FROM groups INNER JOIN faculties ON groups.faculty_id = faculties.id
                                         WHERE faculties.title = '$faculty'
                                         AND groups.course = $cource
                                         AND groups.form_education = '$formEducation'
                                         GROUP BY groups.title LIMIT 5 OFFSET $offset");
    foreach ($keys as $item){
        array_push($bufferArr, ['text' => $item['title'], 'callback_data' => $item['title']]);
    }
    array_push($arr, $bufferArr);
    if($bufferArr != null && 5 == count($bufferArr)){
        $offset = $offset+5;
        return addKeys($chatId, $arr, $offset);
    }
    return $arr;
}

